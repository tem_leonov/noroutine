package ratelimiter

import (
	"sync/atomic"
	"time"
)

type atomicRateLimiter struct {
	next       *atomic.Int64
	interval   int64
	maxNextLag int64
	start      time.Time
}

func newAtomic(rps uint, slack uint) *atomicRateLimiter {
	interval := int64(time.Second) / int64(rps)
	rl := &atomicRateLimiter{
		interval:   interval,
		maxNextLag: int64(slack) * interval,
		next:       &atomic.Int64{},
	}
	rl.next.Store(0)
	return rl
}

func (r *atomicRateLimiter) Wait() {
	var (
		next    int64
		elapsed int64
	)
	for {
		next = r.next.Load()
		if next == 0 {
			r.start = time.Now()
		}

		elapsed = time.Since(r.start).Nanoseconds()
		newNext := next + r.interval
		minNext := elapsed - r.maxNextLag
		if newNext < minNext {
			newNext = minNext
		}
		if r.next.CompareAndSwap(next, newNext) {
			break
		}
	}

	if next > elapsed {
		time.Sleep(time.Duration(next - elapsed))
	}
}
