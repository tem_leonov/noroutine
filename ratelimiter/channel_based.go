package ratelimiter

import (
	"context"
	"sync"
	"time"
)

type channelBasedRateLimiter struct {
	rps uint

	sig chan struct{}

	wg     *sync.WaitGroup
	cancel context.CancelFunc
}

func newChannelBased(ctx context.Context, rps uint, slack uint) *channelBasedRateLimiter {
	var wg sync.WaitGroup
	rl := &channelBasedRateLimiter{
		rps: rps,

		wg: &wg,
	}
	rl.start(ctx, slack)
	return rl
}

func (r *channelBasedRateLimiter) Wait() {
	<-r.sig
}

func (r *channelBasedRateLimiter) Close() {
	r.cancel()
	r.wg.Wait()
}

func (r *channelBasedRateLimiter) start(ctx context.Context, slack uint) {
	ctx, cancel := context.WithCancel(ctx)
	r.cancel = cancel
	r.sig = make(chan struct{}, slack)
	r.wg.Add(1)
	go func() {
		defer r.wg.Done()
		r.loop(ctx)
	}()
}

const period = 10 * time.Millisecond

func (r *channelBasedRateLimiter) loop(ctx context.Context) {
	interval := time.Second / time.Duration(r.rps)
	var (
		nextTick = time.Now()
	)
	for {
		select {
		case <-ctx.Done():
			return
		default:
			for !nextTick.After(time.Now()) {
				select {
				case <-ctx.Done():
					return
				case r.sig <- struct{}{}:
				}
				nextTick = nextTick.Add(interval)
			}
			time.Sleep(period)
		}
	}
}
