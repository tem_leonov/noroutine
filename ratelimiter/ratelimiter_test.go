package ratelimiter

import (
	"context"
	"fmt"
	"testing"
	"time"

	"go.uber.org/ratelimit"
)

func BenchmarkRateLimiterWithSlack(b *testing.B) {
	ctx := context.Background()

	var (
		testTime = 5 * time.Second
		profiles = []uint{1_000_000, 10_000_000, 15_000_000, 20_000_000, 30_000_000}
	)
	for _, rps := range profiles {
		b.Run(fmt.Sprintf("uber %d RPS", rps), func(b *testing.B) {
			t := time.NewTimer(testTime)
			defer t.Stop()
			rl := ratelimit.New(int(rps), ratelimit.WithSlack(int(rps)))
			var reqs uint
		loop:
			for {
				rl.Take()
				select {
				case <-t.C:
					break loop
				default:
					reqs++
				}
			}
			avgRPS := float64(reqs) / (float64(testTime) / float64(time.Second))
			b.ReportMetric(avgRPS, "avg_rps")
			b.ReportMetric(float64(rps), "expected_rps")
		})
		b.Run(fmt.Sprintf("noroutine native %d RPS", rps), func(b *testing.B) {
			t := time.NewTimer(testTime)
			defer t.Stop()
			rl := New(ctx, rps)
			defer rl.Close()
			var reqs uint
		loop:
			for {
				rl.Wait()
				select {
				case <-t.C:
					break loop
				default:
					reqs++
				}
			}
			avgRPS := float64(reqs) / (float64(testTime) / float64(time.Second))
			b.ReportMetric(avgRPS, "avg_rps")
			b.ReportMetric(float64(rps), "expected_rps")
		})
		b.Run(fmt.Sprintf("noroutine atomic %d RPS", rps), func(b *testing.B) {
			t := time.NewTimer(testTime)
			defer t.Stop()
			rl := NewAtomic(ctx, rps, rps)
			var reqs uint
		loop:
			for {
				rl.Wait()
				select {
				case <-t.C:
					break loop
				default:
					reqs++
				}
			}
			avgRPS := float64(reqs) / (float64(testTime) / float64(time.Second))
			b.ReportMetric(avgRPS, "avg_rps")
			b.ReportMetric(float64(rps), "expected_rps")
		})
	}
}

func BenchmarkRateLimiterUberCPU(b *testing.B) {
	var (
		testTime = 5 * time.Second
		rps      = 25_000_000
	)
	b.Run(fmt.Sprintf("uber %d RPS", rps), func(b *testing.B) {
		t := time.NewTimer(testTime)
		defer t.Stop()
		rl := ratelimit.New(int(rps), ratelimit.WithSlack(int(rps)))
		var reqs uint
	loop:
		for {
			rl.Take()
			select {
			case <-t.C:
				break loop
			default:
				reqs++
			}
		}
		avgRPS := float64(reqs) / (float64(testTime) / float64(time.Second))
		b.ReportMetric(avgRPS, "avg_rps")
		b.ReportMetric(float64(rps), "expected_rps")
	})
}
