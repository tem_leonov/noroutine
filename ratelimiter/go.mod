module gitlab.com/tem_leonov/noroutine/ratelimiter

go 1.19

require (
	github.com/benbjohnson/clock v1.3.0 // indirect
	go.uber.org/ratelimit v0.3.0 // indirect
)
