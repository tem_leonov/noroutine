package ratelimiter

type RateLimiter interface {
	Wait()
}

func New(rps, slack uint) RateLimiter {
	return newAtomic(rps, slack)
}
